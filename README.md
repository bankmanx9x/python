## aws
## sql, postgres, mysql
## docker, docker compose (airflow)
## python
## etl, airflow
## data warehouse, redshift
## kafka
## bi tools, power bi, looker

### Project
#### ML - market basket retail
#### Analyst - Airbnb price analyst
#### ML - Regression real estate price
#### ML - Churn, A/B Testing
#### ML - hyperparameter tuning / feature selection
#### ETL pipeline with postgrest / mysql / mssql as source
#### ETL pipeline with S3 / GCS as source 
#### ETL pipeline with pyspark / databricks
#### ELT with databricks and github repo

## Channel: datarockie, prasertcbs
## https://quay.io/repository/astronomer/astro-runtime?tab=info
